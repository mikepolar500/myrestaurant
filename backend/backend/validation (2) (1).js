
/**
 * Validates all the body_params that are used in regester.
 * @param {1} body_params for which all the params has to be verified.
 * @returns true - if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails. 
 */
 function validate_register_data(body_params) {

    // checking every param if anyone of them is not provided-'406' :not acceptable
    if (Object.keys(body_params).length !== 4){

        //if required number of params are not there it will send 421 status code
        
        return {status_code:406, status_message:"Dismatch count"};
    }
    let body_keys_list = []
    for(let item in body_params){
        body_keys_list.push(item)
    }
    //delaring a list of keys to validate the keys
    let key_list = ["fullname","username","mail","password"];
    for (let key of key_list) {
        if (body_keys_list.includes(key)) {
            continue;
        }
        else {
            let msg = key + " is Missing!";
            return {status_code:406, status_message:msg};
        }
    }
    let {fullname,username,mail,password,} = body_params;
    //validating the password using _validate_name()
    let name_response = _validate_name(fullname);
    if (name_response.status_code !== 200) {
        return name_response;
    }
    //validating the password using _validate_password()
    let password_response = _validate_password(password);
    if (password_response.status_code !== 200) {
        return password_response;
    }
   
    //validating the username using _validate_name()
    let username_response = _validate_name(username);
    if (username_response.status_code !== 200) {
        return username_response;
    }
    //validating the email from validate_email
    let validate_email_response = validate_email(mail);
    if (validate_email_response.status_code !== 200) {
        return validate_email_response;
    }
   
    return {status_code:200};
}

/**
 * To validate the name given by the user for registering
 * @param {1} name used to validate various conditions 
 * @returns true - if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails.            
 */
function _validate_name(fullname) {
    if (typeof(fullname)!=="string") { 
        return {status_code:406, status_message:"fullname/username should be a string!"};
    }
    if ((fullname.length < 5) || (fullname.length > 20)) {
        return {status_code:411, status_message:"Length of fullname/username should be 5 to 20"};
    }
    for (let index=0; index<fullname.length-1; index++) {
        if (fullname[index] === " " && fullname[index+1] === " ") {
            return {status_code:406, status_message:"fullname/username should not contain consecutive spaces!"};
        }
    }
    return {status_code:200};
}

/**
 * validates the password for various conditions
 * @param {1} password which has to be validated 
 * @returns true - if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails.  
 */
function _validate_password(password) {
    if (typeof(password) !== "string") {
        return {status_code:406, status_message:"Password should be a string!"};
    }
    if ((password.length < 3) || (password.length > 15)) {
        return {status_code:411, status_message:"Length of password should be in between 3 and 15"};
    }
    let special_chars=["!","@","#","$","%","^","&","*"];
    var special_len=0;
    for (let item of password){
        if (special_chars.includes(item)) {
            special_len += 1;
        }
    }
    if (special_len ===0) {
        return {status_code:406, status_message:"Password should contain atleast one special character"};
    }
    return {status_code:200};
}


function validate_email(mail){
    if(!(mail.endsWith("@gmail"))){
        return {status_code:406,status_message:"entered email is invalid"}
    }else{
        return {status_code:200,status_message:"success"}
    }
}

//..........................................login................................................


function validate_login_data(body_params) {

    // checking every param if anyone of them is not provided-'406' :not acceptable
    if (Object.keys(body_params).length !== 2){

        //if required number of params are not there it will send 421 status code
        
        return {status_code:406, status_message:"Dismatch count"};
    }
    let body_keys_list = []
    for(let item in body_params){
        body_keys_list.push(item)
    }
    //delaring a list of keys to validate the keys
    let key_list = ["username","password"];
    for (let key of key_list) {
        if (body_keys_list.includes(key)) {
            continue;
        }
        else {
            let msg = key + " is Missing!";
            return {status_code:406, status_message:msg};
        }
    }
    let {username,password} = body_params;
  
   
    //validating the email from validate_username
    let validate_username_response = validate_username(username);
    if (validate_username_response.status_code !== 200) {
        return validate_username_response;
    }
     
    //validating the password using _validate_password()
    let password_response = _validate_password(password);
    if (password_response.status_code !== 200) {
        return password_response;
    }
   
    return {status_code:200};
}

module.exports =  {validate_register_data,validate_login_data,_validate_password,_validate_name,validate_email};