
const express = require("express")
//import the mongoose 

const mongoose = require('mongoose');

//connecting to mongoose by using the url that was produced atlas mongo instance

// give password to that link

mongoose.connect('url');


// schema of the user signup

const SIGNUP_PAGE_SCHEMA  = new mongoose.Schema({ 
    fullname: String,
    username: String,
    mail:String,
    password: String,
    required:true
})

// schema of the login user data

const LOGIN_PAGE_SCHEMA  = new mongoose.Schema({
    username:String,
    password:String,
    required:true
})



// forgot_password_schema is used to verify data
const FORGOT_PASSWORD_SCHEMA = mongoose.Schema({
    registered_mail:String,
    required:true
                                         })


// otp_schema is used to verify the mail
const OTP_SCHEMA = mongoose.Schema({
    otp:String,
    required:true 
                                                })



// reset_password_schema is used to create a new password
const RESET_PASSWORD_SCHEMA = mongoose.Schema({
    new_password:String,
    confirm_password:String,
    required:true
                                            })
//here we export all schemas

module.exports = {
    SIGNUP_PAGE_SCHEMA,
    LOGIN_PAGE_SCHEMA,
    FORGOT_PASSWORD_SCHEMA,
    OTP_SCHEMA,
    RESET_PASSWORD_SCHEMA

}





