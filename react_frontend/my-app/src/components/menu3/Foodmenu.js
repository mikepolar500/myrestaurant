
import React from 'react';
import './menu3.css'
class Foodmenu extends React.Component{
    render(){
      const {foodmenu_data} = this.props
        return( 


<div>

<section id="food-menu">
      <h2 class="food-menu-heading">Food Menu</h2>
      <div className="food-menu-container container">
        <div className="food-menu-item">
          <div className="food-img">
            <img src={foodmenu_data.img9} alt="" />
          </div>
          <div className="food-description">
            <h2 class="food-titile">Food Menu Item 1</h2>
            <p>
              Hyderabadi biryani, also known as Hyderabadi dum biryani, is a style of biryani from Hyderabad, India made with basmati rice and goat meat .
            </p>
            <p class="food-price">Price: &#8377; 250</p>
          </div>
        </div>

        <div className="food-menu-item">
          <div className="food-img">
            <img
              src={foodmenu_data.img10}
              alt="error"
            />
          </div>
          <div class="food-description">
            <h2 class="food-titile">Food Menu Item 2</h2>
            <p>
              Better to be deprived of food for three days, than tea for one." ... "To the ruler, the people are heaven; to the people, food is heaven.
            </p>
            <p class="food-price">Price: &#8377; 250</p>
          </div>
        </div>
        <div className="food-menu-item">
          <div className="food-img">
            <img src={foodmenu_data.img11} alt="" />
          </div>
          <div className="food-description">
            <h2 class="food-titile">Food Menu Item 3</h2>
            <p>
              The vegetarian dishes consist of koora, which include cooking different vegetables in a variety of styles - with gravy, frying, with lentils, etc.
            </p>
            <p class="food-price">Price: &#8377; 110</p>
          </div>
        </div>
        <div className="food-menu-item">
          <div className="food-img">
            <img src={foodmenu_data.img12} alt="" />
          </div>
          <div className="food-description">
            <h2 class="food-titile">Food Menu Item 4</h2>
            <p>
              This collection of tempting non vegetarian starters and snacks comes with the promise to tantalise the taste buds just so. . . Reading books is a kind of enjoyment. Reading books is a good habit
            </p>
            <p class="food-price">Price: &#8377; 349</p>
          </div>
        </div>
        <div className="food-menu-item">
          <div className="food-img">
            <img src={foodmenu_data.img13} alt="" />
          </div>
          <div className="food-description">
            <h2 class="food-titile">Food Menu Item 5</h2>
            <p>
              The Afghani Chicken Kebabs are made from minced meat of boneless tender cuts, and seasoned with spices and herbs
            </p>
            <p class="food-price">Price: &#8377; 199</p>
          </div>
        </div>
        <div className="food-menu-item">
          <div className="food-img">
            <img src={foodmenu_data.img132} alt="" />
          </div>
          <div className="food-description">
            <h2 class="food-titile">Food Menu Item 6</h2>
            <p>
              Really fun cocktail class with Kay - really focused on the details of every drink, learnt a lot of small tips and tricks of the trade. Cocktails were great too.
            </p>
            <p class="food-price">Price: &#8377; 249</p>
          </div>
        </div>
      </div>
    </section>
</div>
        )
    }}
export default Foodmenu;