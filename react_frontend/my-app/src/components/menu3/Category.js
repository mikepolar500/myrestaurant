import React from 'react';
import './menu3.css';
class Category extends React.Component{
    render(){
      const {category_data} = this.props
        return(

<div>
  <section id="food">
 <h2>Types of food</h2>
  <div className="food-container container">
  <div className="food-type fruite">
    <div className="img-container">
      <img src={category_data.img17} alt="error" />
      <div className="img-content">
        <h3>Hyd_Dum_Biryani</h3>
        <a
          href="https://www.sanjeevkapoor.com/Recipe/Hyderabadi-Biryani-KhaanaKhazana.html"
          class="btn btn-primary"
          target="blank"
          >learn more</a
        >
      </div>
    </div>
  </div>
  <div className="food-type vegetable">
    <div className="img-container">
      <img src={category_data.img18} alt="error" />
      <div className="img-content">
        <h3>Chinese_food</h3>
        <a
          href="https://thewoksoflife.com/chinese-pantry-essential-ingredients/"
          class="btn btn-primary"
          target="blank"
          >learn more</a
        >
      </div>
    </div>
  </div>
  <div className="food-type grin">
    <div className="img-container">
      <img src={category_data.img19} alt="error" />
      <div className="img-content">
        <h3>Veg_spicy</h3>
        <a
          href="https://www.greatbritishchefs.com/collections/vegetarian-recipes"
          class="btn btn-primary"
          target="blank"
          >learn more</a
        >
      </div>
    </div>
  </div>
 </div>
 </section>
</div>
);
}}



export default Category;