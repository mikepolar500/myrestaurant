import React from 'react';
import './menu3.css'
class Navbar extends React.Component{
    render(){
        return( 
<div>
    <nav class="navbar">
      <div className="navbar-container container">
          <input type="checkbox" name="" id=""/>
          <div className="hamburger-lines">
              <span class="line line1"></span>
              <span class="line line2"></span>
              <span class="line line3"></span>
          </div>
          <ul class="menu-items">
              <li><a href="#home">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#food">Category</a></li>
              <li><a href="#food-menu">Menu</a></li>
              <li><a href="#testimonials">Testimonial</a></li>
              <li><a href="#contact">Contact</a></li>
          </ul>
          <h1 class="logo">RS</h1>
      </div>
  </nav>
    <section class="showcase-area" id="showcase">
      <div className="showcase-container">
        <h1 class="main-title" id="home">Spicy_kitchen</h1>
        <p>Eat Healty, it is good for our health.</p>
        <a href="#food-menu" class="btn btn-primary">Menu</a>
      </div>
    </section>
    </div>
        )
    }}

export default Navbar;