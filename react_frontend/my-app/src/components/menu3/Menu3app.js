import React from 'react';
import './menu3.css'

import Navbar from './Navbar.js';
import Contact from './Contact.js';
import Aboutus from './Aboutus.js';
import Category from './Category.js';
import Foodmenu from './Foodmenu.js';
import Testimonals from './Testimonals.js';

import Footer from './Footer';

import { about_data, contact_data,category_data, foodmenu_data, testimonials_data } from '../images data/data';



function Menu3app() {
  return (
      <>
    <Navbar/>
    <Aboutus about_data = {about_data}/>
    <Category category_data={category_data}/>
    <Foodmenu foodmenu_data = {foodmenu_data}/>
    <Testimonals testimonals_data = {testimonials_data}/>
    <Contact contact_data = {contact_data}/>
    <Footer/>

    </>
  );
}

export default Menu3app;
