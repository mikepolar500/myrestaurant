
import React from 'react';
import './menu2.css'
class Testimonals extends React.Component{
    render(){
      const {testimonials_data} = this.props
return(


<div>

<section id="testimonials">
      <h2 class="testimonial-title">What Our Customers Say</h2>
      <div className="testimonial-container container">
        <div className="testimonial-box">
          <div className="customer-detail">
            <div className="customer-photo">
              <img src={testimonials_data.img14} alt="" />
              <p class="customer-name">Pradeep_reddy</p>
            </div>
          </div>
          <div className="star-rating">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
          </div>
          <p class="testimonial-text">
            Came for lunch with my sister. We loved our Thai-style mains which were amazing with lots of flavour, very impressive for a Non-vegetarian and Veg restaurant.
          </p>
         
        </div>
        <div className="testimonial-box">
          <div className="customer-detail">
            <div className="customer-photo">
              <img
                src={testimonials_data.img15}
                alt=""
              />
              <p class="customer-name">Deva_sena</p>
            </div>
          </div>
          <div className="star-rating">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
          </div>
          <p class="testimonial-text">
            I love the service at the place and the chef is so friendly with the guests and always takes care to offer the best quality! I highly recommend this place.
          </p>
         
        </div>
        <div className="testimonial-box">
          <div className="customer-detail">
            <div className="customer-photo">
              <img src={testimonials_data.img16} alt="" />
              <p class="customer-name">Vinay</p>
            </div>
          </div>
          <div className="star-rating">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
          </div>
          <p class="testimonial-text">
            Amaazing food! The whole experience from start to finish is great waitress is always so friendly and kind. The food can’t get better and the prices are fair for the portion size. Always a great spot to get great food
          </p>
         
        </div>
      </div>
    </section>
</div>


)
    }}
export default Testimonals;
