import{Link} from 'react-router-dom';
import React from 'react';

import './forgotpass.css';

function Forgotpass() {
    return (
    <div>
            <h1>Forgot your password?</h1>
      <hr></hr>
      <h3>Enter your email address to reset your password</h3>
      
      <form action="index.html" method="post">
        <label for="mail">Email</label><br/>
        <input type="email" id="name" name="name" placeholder="Enter your email address" required onblur="validateName(name)"/>  
        < Link to="/otp">
        <button type="submit">Submit</button> </Link>
        <span id="nameError" className="error">There was an error with your email</span>
      </form> 
    </div>

    );
}
export default Forgotpass;
