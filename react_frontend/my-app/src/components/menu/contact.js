
import React from 'react';
import './menu.css';

class Contact extends React.Component {
  render() {
    const {contact_data} = this.props
  return (
    <section id="contact">
      <div className="contact-container container">
        <div className="contact-img">
          <img src={contact_data.img5} alt="" />
        </div>

        <div className="form-container">
          <h2>Contact Us</h2>
          <input type="text" placeholder="Your Name" />
          <input type="email" placeholder="E-Mail" />
          <textarea
            cols="30"
            rows="6"
            placeholder="Type Your Message"
          ></textarea>
          <a href="/#" class="btn btn-primary">Submit</a>
        </div>
      </div>
    </section>
  );
}
}

export default Contact;
