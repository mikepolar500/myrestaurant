
import React from 'react';
import './menu.css';

class Testimonials extends React.Component {
  render() {
    const {testimonials_data} =this.props
  return (
    <section id="testimonials">
    <h2 className="testimonial-title">What Our Customers Say</h2>
    <div className="testimonial-container container">
      <div className="testimonial-box">
        <div className="customer-detail">
          <div className="customer-photo">
            <img src={testimonials_data.img14} alt="" />
            <p className="customer-name">Pradeep_reddy</p>
          </div>
        </div>
        <div className="star-rating">
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
        </div>
        <p className="testimonial-text">
          Came for lunch with my sister. We loved our Thai-style mains which were amazing with lots of flavour, very impressive for a Non-vegetarian and Veg restaurant.
        </p>
       
      </div>
      <div className="testimonial-box">
        <div className="customer-detail">
          <div className="customer-photo">
            <img
              src={testimonials_data.img15}
              alt=""
            />
            <p className="customer-name">Deva_sena</p>
          </div>
        </div>
        <div className="star-rating">
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
        </div>
        <p className="testimonial-text">
          I love the service at the place and the chef is so friendly with the guests and always takes care to offer the best quality! I highly recommend this place.
        </p>
       
      </div>
      <div className="testimonial-box">
        <div className="customer-detail">
          <div className="customer-photo">
            <img src={testimonials_data.img16} alt="" />
            <p className="customer-name">Vinay</p>
          </div>
        </div>
        <div className="star-rating">
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
        </div>
        <p className="testimonial-text">
          Amaazing food! The whole experience from start to finish is great waitress is always so friendly and kind. The food can’t get better and the prices are fair for the portion size. Always a great spot to get great food
        </p>
       
      </div>
    </div>
  </section>
  );
  }
}

export default Testimonials;
