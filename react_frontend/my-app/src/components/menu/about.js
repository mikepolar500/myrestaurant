
import React from 'react';
import './menu.css';

class About extends React.Component {
  render() {
    const {about_data} = this.props
  return (
    <section id="about">
      <div class="about-wrapper container">
        <div class="about-text">
          <p class="small">About Us</p>
          <h2>We've beem making healthy food last for 10 years</h2>
          <p>
            Healthy food is real and whole; wouldn’t you rather be and feel real and whole than fake? “Your body is a temple, but only if you treat it as one.” ~Astrid Alauda This healthy eating quote is so important. Teach it to your children, especially older children to help with their self esteem and to help them eat better.
          </p>
        </div>
        <div class="about-img">
          <img src= {about_data.img4}alt="food" />
        </div>
      </div>
    </section>
  );
}
}

export default About;
