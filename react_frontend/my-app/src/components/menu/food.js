
import React from 'react';
import './menu.css';

class Food extends React.Component {
  render() {
    const {food_data} = this.props
  return (
    <section id="food">
      <h2>Types of food</h2>
      <div className="food-container container">
        <div className="food-type fruite">
          <div className="img-container">
            <img src={food_data.img6} alt="error" />
            <div className="img-content">
              <h3>Hyd_Dum_Biryani</h3>
              <a
                href="https://www.sanjeevkapoor.com/Recipe/Hyderabadi-Biryani-KhaanaKhazana.html"
                className="btn btn-primary"
                target="blank"
                >learn more</a
              >
            </div>
          </div>
        </div>
        <div className="food-type vegetable">
          <div className="img-container">
            <img src={food_data.img7} alt="error" />
            <div className="img-content">
              <h3>Chinese_food</h3>
              <a
                href="https://thewoksoflife.com/chinese-pantry-essential-ingredients/"
                className="btn btn-primary"
                target="blank"
                >learn more</a
              >
            </div>
          </div>
        </div>
        <div className="food-type grin">
          <div className="img-container">
            <img src={food_data.img8} alt="error" />
            <div className="img-content">
              <h3>Veg_spicy</h3>
              <a
                href="https://www.greatbritishchefs.com/collections/vegetarian-recipes"
                className="btn btn-primary"
                target="blank"
                >learn more</a
              >
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
}

export default Food;
