import React from 'react';
import Navbar from './nav';
import Showcase from './showcase';
import About from './about.js';
import Food from './food.js';
import Foodmenu from './foodmenu';
import Testimonials from './testimonials.js';
import Contact from './contact.js';
import Footer from './footer.js';
import {about_data, testimonials_data, contact_data, foodmenu_data, footer_data} from '../images data/data';




function MenuApp() {
  return (
    <>
    <Navbar/>
    <Showcase/>
    <About about_data={about_data}/>
    <Food />
    <Foodmenu foodmenu_data= {foodmenu_data}/>
    <Testimonials testimonials_data = {testimonials_data}/>
    <Contact contact_data={contact_data}/>
    <Footer footer_data= {footer_data}/>
    </>
  );
}

export default MenuApp;
