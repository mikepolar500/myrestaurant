
import React from 'react';
import './menu.css';

class Showcase extends React.Component {
  render() {
  return (
    <section className="showcase-area" id="showcase">
      <div className="showcase-container">
        <h1 className="main-title" id="home">Spicy_kitchen</h1>
        <p>Eat Healty, it is good for our health.</p>
        <a href="#food-menu" className="btn btn-primary">Menu</a>
      </div>
    </section>
  );
}
}

export default Showcase;
