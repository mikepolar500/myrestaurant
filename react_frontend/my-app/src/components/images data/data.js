const footer_data = {
    img:"https://image.shutterstock.com/z/stock-photo-hot-spicy-chicken-biryani-a-most-famous-food-of-pakistani-indian-peoples-195628511.jpg",
    img1:"https://wallpaperaccess.com/full/1972908.jpg",
    img2:"https://th.bing.com/th/id/OIP.dYp5tkHL8xOf6dKI_BpqZQHaE8?pid=ImgDet&rs=1"
}

const header_data = {
    img3:"https://i.ibb.co/RPCShfH/tranquil-escape.jpg"
}

const about_data = {
    img4:"https://www.sunshinebusiness.com.au/wp-content/uploads/2020/04/94E06A02-1027-4A10-8A2F-79803B199813.jpeg"
}

const contact_data = {
    img5:"https://th.bing.com/th/id/R.743880d96664490e21b25f65be9f318e?rik=21GeL2vfb9o0jA&riu=http%3a%2f%2fweknowyourdreams.com%2fimages%2frestaurant%2frestaurant-12.jpg&ehk=Zls%2fSDvXC7GdzARTQ0LsvNH93UDuDeu2ndoyjQDNr6Y%3d&risl=&pid=ImgRaw&r=0"
}

const food_data = {
    img6:"https://th.bing.com/th/id/OIP.gInjRBrjpkAif2iR9uC2wQHaGL?pid=ImgDet&rs=1",
    img7:"https://th.bing.com/th/id/OIP.RWxHHdgNaidR3t5IX6hkmAHaE8?pid=ImgDet&rs=1",
    img8:"https://external-preview.redd.it/EJpz-SwKT4yp0gKZCqEd3cZlZGYI2KtuFl0dZ9Q-31c.jpg?auto=webp&s=41b2fe828c57bb8c79319a0ad8cdebb717875dc5"
}

const foodmenu_data = {
    img9:"https://www.myyellowplate.com/wp-content/uploads/2018/12/Echoes-Best-Tandoori-Momos-1.jpg",
    img10:"https://th.bing.com/th/id/R.39324099db9a95124b9aa82cedd84992?rik=432YvPX1uNOGLg&riu=http%3a%2f%2fswatisani.net%2fkitchen%2fwp-content%2fuploads%2f2016%2f04%2fCollage_food.jpg&ehk=3S1O40haLFn3eWbPkq0WN8bnYq3kBuOpXFIYAZnBNIc%3d&risl=&pid=ImgRaw&r=0",
    img11:"https://th.bing.com/th/id/OIP.XWPUSLJUGdwL-97FJbx8qwHaFL?w=240&h=180&c=7&r=0&o=5&dpr=1.5&pid=1.7",
    img12:"https://th.bing.com/th/id/OIP.wl_Sp8XtveK_JpDucAKkQQHaE_?pid=ImgDet&rs=1",
    img13:"https://complex-res.cloudinary.com/images/w_1200/fcdmcuygvonsu8lmrzv9/fruit"
}

const testimonials_data = {
    img14:"https://i.postimg.cc/5Nrw360Y/male-photo1.jpg",
    img15:"https://i.postimg.cc/sxd2xCD2/female-photo1.jpg",
    img16:"https://i.postimg.cc/fy90qvkV/male-photo3.jpg"
}
const category_data = {

    img17:"https://www.shanazrafiq.com/wp-content/uploads/2019/02/Dum-Biryani-7.jpg",

    img18:"https://cdn-image.myrecipes.com/sites/default/files/styles/4_3_horizontal_-_1200x900/public/1502987547/GettyImages-545286388.jpg?itok=kGrhiiMo",

    img19:"https://th.bing.com/th/id/OIP.paGYLcH6IrP9qQ3rO7AS7QHaE8?pid=ImgDet&rs=1"

}

export {footer_data, header_data, contact_data,category_data, foodmenu_data, testimonials_data, about_data, food_data};